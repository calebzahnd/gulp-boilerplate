# [Gulp Boilerplate](https://bitbucket.org/calebzahnd/gulp-boilerplate)

A quick starter to get the Gulp taskrunner working in your project. This is a basic boilerplate that you can easily add to. Out of the box it automates many web dev tasks such as cleaning directories, compiling SASS, concatenating and minifying SASS and js, linting js, compressing images, etc.

### Getting Started

Gulp requires [NodeJS](http://nodejs.org) in order to run. [NVM](https://github.com/creationix/nvm) is a super easy and flexible way to get started with Node.

***

#### Installing Node Modules

After downloading or cloning this repo, you will need to install the necessary Node Modules using a simple npm command `npm install`. You can view the necessary dependencies by cracking open the included package.json file.

#### Defining tasks with Gulp

If you're at all familiar with GruntJS, you'll feel right at home looking at Gulp. Think of it this way: the taskrunner "Gulps" your files down, and then sends them through various "pipes" to run defined tasks on them, before spitting them back out at their destination.

Inside gulpfile.js, you'll see we first load our required plugins, and then we setup a simple list of defined actions. In this particular Boilerplate, I've limited it to simply working with SASS, Javascript, Image Compressing, and Directory cleaning.

You can call a task directy in the terminal by entering gulp and the taskname, like `gulp styles`, or `gulp clean`.

#### Examples
`gulp` :: Typically, you'll probably run gulp as a watch task. This will begin to watch your defined directories for changes in .scss, .js, and image files. Once it sees a change, it will run the predefined task automagically, while you continue to code. Pretty sweet, right?

***

I make no qualifications for this boilerplate being bug free. Let me know how I can improve it!